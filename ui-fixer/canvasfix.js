var dialogs = document.getElementsByClassName( "ui-dialog" );
for ( var i = 0; i < dialogs.length; i++ )
{
    dialogs[i].style.setProperty( "width", "90%" );
    dialogs[i].style.setProperty( "left", "5%" );
}

var inputs = document.querySelectorAll( "form" );
for ( var i = 0; i < inputs.length; i++ )
{
    inputs[i].style.setProperty( "width", "90%" );
}

var inputs = document.querySelectorAll( "table" );
for ( var i = 0; i < inputs.length; i++ )
{
    inputs[i].style.setProperty( "width", "90%" );
}

var inputs = document.querySelectorAll( "input" );
for ( var i = 0; i < inputs.length; i++ )
{
    inputs[i].style.setProperty( "width", "90%" );
}

inputs = document.getElementsByClassName( "input-append" );
for ( var i = 0; i < inputs.length; i++ )
{
    inputs[i].style.setProperty( "width", "100%" );
}
